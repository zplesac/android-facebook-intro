package co.infinum.facebookintro.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.infinum.facebookintro.R;
import co.infinum.facebookintro.utils.DialogUtils;
import co.infinum.facebookintro.utils.ProjectConfig;

/**
 * Created by zeljkoplesac on 01/07/14.
 */
public class MainActivity extends ActionBarActivity {

    /**
     * Facebook UI lifecycle helper.
     * This class helps to create, automatically open (if applicable), save, and restore the Active Session
     */
    private UiLifecycleHelper uiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        uiHelper = new UiLifecycleHelper(MainActivity.this, callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        uiHelper.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                setSupportProgressBarIndeterminateVisibility(false);
                DialogUtils.buildAlertDialog(MainActivity.this, getString(R.string.app_name), getString(R.string.error_share_on_facebook));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                String completionGesture = FacebookDialog.getNativeDialogCompletionGesture(data);
                setSupportProgressBarIndeterminateVisibility(false);

                if (completionGesture.equalsIgnoreCase(ProjectConfig.FACEBOOK_PICTURE_POSTED)) {
                    // content has been shared
                    DialogUtils.buildAlertDialog(MainActivity.this, getString(R.string.app_name), getString(R.string.post_published));
                } else {
                    // User clicked the Cancel button
                    DialogUtils.buildAlertDialog(MainActivity.this, getString(R.string.app_name), getString(R.string.error_publish_cancelled));
                }
            }
        });

    }

    @OnClick(R.id.share)
    void onShareClick() {
        if (FacebookDialog.canPresentShareDialog(getApplicationContext(),
                FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {

            // Publish the post using the Share Dialog
            FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
                    .setLink("https://developers.facebook.com/android")
                    .setApplicationName(getString(R.string.app_name))
                    .build();

            setSupportProgressBarIndeterminateVisibility(true);
            uiHelper.trackPendingDialogCall(shareDialog.present());

        } else {
            // Fallback. For example, publish the post using the Feed Dialog
            onWebDialogClick();
        }
    }

    @OnClick(R.id.upload)
    void onUploadClick() {
        if (FacebookDialog.canPresentShareDialog(getApplicationContext(),
                FacebookDialog.ShareDialogFeature.PHOTOS)) {

            Bitmap image = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_launcher);

            ArrayList<Bitmap> pictures = new ArrayList<Bitmap>();
            pictures.add(image);

            // Publish the post using the Share Dialog
            FacebookDialog shareDialog = new FacebookDialog.PhotoShareDialogBuilder(this)
                    .addPhotos(pictures)
                    .setApplicationName(getString(R.string.app_name))
                    .build();

            setSupportProgressBarIndeterminateVisibility(true);
            uiHelper.trackPendingDialogCall(shareDialog.present());
        } else {
            // Fallback.
        }
    }

    @OnClick(R.id.web_dialog)
    void onWebDialogClick() {
        Bundle params = new Bundle();
        params.putString("name", "Facebook SDK for Android");
        params.putString("caption", "Build great social apps and get more installs.");
        params.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
        params.putString("link", "https://developers.facebook.com/android");
        params.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

        WebDialog feedDialog = (
                new WebDialog.FeedDialogBuilder(MainActivity.this,
                        Session.getActiveSession(),
                        params))
                .setOnCompleteListener(new WebDialog.OnCompleteListener() {

                    @Override
                    public void onComplete(Bundle values,
                                           FacebookException error) {
                        if (error == null) {
                            // When the story is posted, echo the success
                            final String postId = values.getString(ProjectConfig.EXTRA_POST_ID);
                            if (postId != null) {
                                Toast.makeText(MainActivity.this,
                                        getString(R.string.post_published),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                // User clicked the Cancel button
                                Toast.makeText(MainActivity.this,
                                        getString(R.string.error_publish_cancelled),
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else if (error instanceof FacebookOperationCanceledException) {
                            // User clicked the "x" button
                            Toast.makeText(MainActivity.this,
                                    getString(R.string.error_publish_cancelled),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Generic, ex: network error
                            Toast.makeText(MainActivity.this,
                                    getString(R.string.error_posting_story),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                }).build();
        feedDialog.show();
    }

    @OnClick(R.id.graph_api)
    void onGraphAPIClicked(){
        if(Session.getActiveSession().isOpened()){
            // Request user data and show the results
            Request.executeMeRequestAsync(Session.getActiveSession(), new Request.GraphUserCallback() {

                @Override
                public void onCompleted(GraphUser user, Response response) {
                    if (user != null) {
                        String text = "Hello " + user.getFirstName() + " " + user.getLastName();
                        Toast.makeText(MainActivity.this,
                                text,
                                Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }

    /**
     * Provides asynchronous notification of Session state changes.
     */
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isClosed()) {
            // fallback to login
            finish();
        }
    }
}
