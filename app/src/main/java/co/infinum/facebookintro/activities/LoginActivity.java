package co.infinum.facebookintro.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.OnClick;
import co.infinum.facebookintro.R;
import co.infinum.facebookintro.utils.DialogUtils;
import co.infinum.facebookintro.utils.Utils;

/**
 * Created by zeljkoplesac on 01/07/14.
 */
public class LoginActivity extends ActionBarActivity {

    /**
     * Facebook UI lifecycle helper.
     * This class helps to create, automatically open (if applicable), save, and restore the Active Session
     */
    private UiLifecycleHelper uiHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        uiHelper = new UiLifecycleHelper(LoginActivity.this, callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @OnClick(R.id.login)
    void onLoginButtonClick(){

        // check for valid internet connection
        if (Utils.checkInternetConnection(LoginActivity.this)) {
            Session session = Session.getActiveSession();

            // check if session is already openned
            if (!session.isOpened() && !session.isClosed()) {
                session.openForRead(new Session.OpenRequest(this)
                        .setPermissions(Arrays.asList("public_profile"))
                        .setCallback(callback));
            }
            // session is already active, open it
            else {
                Session.openActiveSession(this, true, callback);
            }
        } else {
            DialogUtils.buildAlertDialog(LoginActivity.this, getString(R.string.app_name), getString(R.string.internet_connection_missing));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // For scenarios where the main activity is launched and user
        // session is not null, the session state change notification
        // may not be triggered. Trigger it if it's open/closed.
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
        ButterKnife.reset(this);
    }

    /**
     * Provides asynchronous notification of Session state changes.
     */
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {

        // session is opened, proceed to main activity
        if (state.isOpened()) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        // session is closed, show error dialog
        else if (state.isClosed()) {
            DialogUtils.buildAlertDialog(LoginActivity.this, getString(R.string.app_name), getString(R.string.logged_out));
        }
    }
}
