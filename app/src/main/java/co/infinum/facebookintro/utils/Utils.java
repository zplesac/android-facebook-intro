package co.infinum.facebookintro.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by zeljkoplesac on 01/07/14.
 */
public class Utils {

    /**
     * Check if internet connection is active.
     *
     * @param activity
     * @return true if active connection, false otherwise
     */
    public static boolean checkInternetConnection(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }
}
