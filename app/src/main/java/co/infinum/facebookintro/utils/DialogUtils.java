package co.infinum.facebookintro.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by zeljkoplesac on 01/07/14.
 */
public class DialogUtils {

    /**
     * Helper method to show alert dialog.
     * @param message Message to be showed
     * @param activity Activy which has contex
     * @param title Title of dialog box
     */

    public static void buildAlertDialog(Activity activity, String title, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title).setMessage(message).setCancelable(false).setPositiveButton(activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();

        if(!activity.isFinishing()){
            alert.show();
        }
    }
}
