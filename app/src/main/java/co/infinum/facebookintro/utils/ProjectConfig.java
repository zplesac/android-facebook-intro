package co.infinum.facebookintro.utils;

/**
 * Created by zeljkoplesac on 01/07/14.
 */
public class ProjectConfig {

    public static final String EXTRA_POST_ID = "post_id";

    public static String FACEBOOK_PICTURE_POSTED = "post";

    public static String FACEBOOK_PICTURE_CANCELED = "cancel";

}
