#Android Facebook Intro
***

An example project demonstrating some of the most popular Facebook for Android API actions:

1. Facebook login
2. Publish to wall by using native Facebook application
3. Publish to wall by using Web Dialog
4. Upload photo
5. Graph API request

Developed for Infinum Android Talks #6.